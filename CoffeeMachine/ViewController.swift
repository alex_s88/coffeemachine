//
//  ViewController.swift
//  CoffeeMachine
//
//  Created by Alexandr Sopilnyak on 18.06.2018.
//  Copyright © 2018 Alexandr Sopilnyak. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    enum AddIngredient: Int {
        case plusSugar
        case minusSugar
        case plusMilk
        case minusMilk
    }
    
    @IBOutlet weak var textViewOutlet: UITextView!
    @IBOutlet weak var sugarCounterOutlet: UITextField!
    @IBOutlet weak var milkCounterOutlet: UITextField!
    
    @IBOutlet weak var espressoButtonOutlet: UIButton!
    @IBOutlet weak var latteButtonOutlet: UIButton!
    @IBOutlet weak var americanoButtonOutlet: UIButton!
    @IBOutlet weak var capuccionoButtonOutlet: UIButton!
    
    @IBOutlet weak var waterButtonOutlet: UIButton!
    @IBOutlet weak var milkButtonOutlet: UIButton!
    @IBOutlet weak var coffeeButtonOutlet: UIButton!
    @IBOutlet weak var sugarButtonOutlet: UIButton!
    @IBOutlet weak var wastesIndicatorOutlet: UILabel!
    
    var additionalIngredientsCount = (milk: 0, sugar: 0)
    
    var coffeeNerds: CoffeeMachine!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        coffeeNerds = CoffeeMachine(water: 1500, coffeeBeans: 250, milk: 500, sugar: 20)
        
    }
    
    @IBAction func addIngredientsButtonPressed(_ sender: UIButton) {
        if let addIngredients = AddIngredient(rawValue: sender.tag) {
            switch addIngredients {
            case .plusSugar:
                additionalIngredientsCount.sugar += 1
                print("Added 1 sugar")
                if additionalIngredientsCount.sugar > 10 {
                    additionalIngredientsCount.sugar = 10
                    textViewOutlet.text = "Number of sugar bricks cannot be larger than 10"
                }
            case .minusSugar:
                additionalIngredientsCount.sugar -= 1
                print("Minus 1 sugar")
                if additionalIngredientsCount.sugar < 0 {
                    additionalIngredientsCount.sugar = 0
                    textViewOutlet.text = "Number of sugar bricks cannot be lower than 0"
                }
            case .plusMilk:
                additionalIngredientsCount.milk += 1
                print("Added 1 milk")
                if additionalIngredientsCount.milk > 20 {
                    additionalIngredientsCount.milk = 20
                    textViewOutlet.text = "Number of milk portions cannot be larger than 20"
                }
            case .minusMilk:
                additionalIngredientsCount.milk -= 1
                print("Minus 1 milk")
                if additionalIngredientsCount.milk < 0 {
                    additionalIngredientsCount.milk = 0
                    textViewOutlet.text = "Number of milk portions cannot be lower than 0"
                }
            }
        }
        sugarCounterOutlet.text = additionalIngredientsCount.sugar.description
        milkCounterOutlet.text = additionalIngredientsCount.milk.description
        
    }
    
    @IBAction func makeEspressoButtonPressed(_ sender: UIButton) {
        coffeeNerds.makeEspresso(sugarBricks: additionalIngredientsCount.sugar, milkPortions: additionalIngredientsCount.milk)
        textViewOutlet.text = "Take your espresso please!"
        updateIndicators()
    }
    
    @IBAction func makeLatteButtonPressed(_ sender: UIButton) {
        coffeeNerds.makeLatte(sugarBricks: additionalIngredientsCount.sugar, milkPortions: additionalIngredientsCount.milk)
        textViewOutlet.text = "Take your latte please!"
        updateIndicators()
    }
    
    @IBAction func makeAmericanoButtonPressed(_ sender: UIButton) {
        coffeeNerds.makeAmericano(sugarBricks: additionalIngredientsCount.sugar, milkPortions: additionalIngredientsCount.milk)
        textViewOutlet.text = "Take your americano please!"
        updateIndicators()
    }
    
    @IBAction func makeCapuccinoButtonPressed(_ sender: UIButton) {
        coffeeNerds.makeCapuccino(sugarBricks: additionalIngredientsCount.sugar, milkPortions: additionalIngredientsCount.milk)
        textViewOutlet.text = "Take your capuccino please!"
        updateIndicators()
    }
    
    @IBAction func waterButtonPressed(_ sender: UIButton) {
        coffeeNerds.addWater()
        updateWaterIndicator()
        activateButtons()
        print("Water button pressed")
        textViewOutlet.text = "Water successfull added! Now make choise."
        
    }
    @IBAction func milkButtonPressed(_ sender: UIButton) {
        coffeeNerds.addMilk()
        updateMilkIndicator()
        activateButtons()
        print("Milk button pressed")
        textViewOutlet.text = "Milk successfull added! Now make choise."
    }
    
    @IBAction func coffeeButtonPressed(_ sender: UIButton) {
        coffeeNerds.addCoffeeBeans()
        updateCoffeeIndicator()
        activateButtons()
        print("Coffee button pressed")
        textViewOutlet.text = "Coffee beans successfull added! Now make choise."
    }
    
    @IBAction func sugarButtonPressed(_ sender: UIButton) {
        coffeeNerds.addSugar()
        updateSugarIndicator()
        activateButtons()
        print("Sugar button pressed")
        textViewOutlet.text = "Sugar successfull added! Now make choise."
    }
    
    @IBAction func cleanButtonPressed(_ sender: UIButton) {
        coffeeNerds.clean()
        activateButtons()
        updateWastesIndicator()
        print("Clean button pressed")
        textViewOutlet.text = "Coffee machine is clean!"
    }
    
    func updateIndicators() {
        updateWaterIndicator()
        updateWastesIndicator()
        updateMilkIndicator()
        updateSugarIndicator()
        updateCoffeeIndicator()
        print("Indicators updated")
    }
    
    func updateWaterIndicator() {
        let water = coffeeNerds.status().0
        switch water {
        case 0:
            waterButtonOutlet.titleLabel?.textColor = UIColor.red
            textViewOutlet.text = "Water container is empty. Please press 'Water' button!"
            print("Water empty")
            deactivateButtons()
        case 1:
            waterButtonOutlet.titleLabel?.textColor = UIColor.orange
            print("Water middle")
        case 2:
            waterButtonOutlet.titleLabel?.textColor = UIColor.green
            print("Water full")
        default:
            print("Unexpected error.")
        }
    }
    
    func updateMilkIndicator() {
        let milk = coffeeNerds.status().1
        switch milk {
        case 0:
            milkButtonOutlet.titleLabel?.textColor = UIColor.red
            textViewOutlet.text = "Milk container is empty. Please press 'Milk' button!"
            print("Milk empty")
            deactivateButtons()
        case 1:
            milkButtonOutlet.titleLabel?.textColor = UIColor.orange
            print("Milk middle")
        case 2:
            milkButtonOutlet.titleLabel?.textColor = UIColor.green
            print("Milk full")
        default:
            print("Unexpected error.")
        }
    }
    
    func updateSugarIndicator() {
        let sugar = coffeeNerds.status().2
        switch sugar {
        case 0:
            sugarButtonOutlet.titleLabel?.textColor = UIColor.red
            textViewOutlet.text = "Sugar container is empty. Please press 'Sugar' button!"
            print("Sugar empty")
            deactivateButtons()
        case 1:
            sugarButtonOutlet.titleLabel?.textColor = UIColor.orange
            print("Sugar middle")
        case 2:
            sugarButtonOutlet.titleLabel?.textColor = UIColor.green
            print("Sugar full")
        default:
            print("Unexpected error.")
        }
    }
    
    func updateCoffeeIndicator() {
        let coffee = coffeeNerds.status().3
        switch coffee {
        case 0:
            coffeeButtonOutlet.titleLabel?.textColor = UIColor.red
            textViewOutlet.text = "Coffee container is empty. Please press 'Coffee' button!"
            print("Coffee beans empty")
            deactivateButtons()
        case 1:
            coffeeButtonOutlet.titleLabel?.textColor = UIColor.orange
            print("Coffee beans middle")
        case 2:
            coffeeButtonOutlet.titleLabel?.textColor = UIColor.green
            print("Coffee beans full")
            
        default:
            print("Unexpected error.")
        }
    }
    
    func updateWastesIndicator() {
        let wastes = coffeeNerds.status().4
        switch wastes {
        case 0:
            wastesIndicatorOutlet.text = "Low"
            wastesIndicatorOutlet.textColor = UIColor.green
            print("Wastes low")
        case 1:
            wastesIndicatorOutlet.text = "Middle"
            wastesIndicatorOutlet.textColor = UIColor.orange
            print("Wastes middle")
        case 2:
            wastesIndicatorOutlet.text = "Full"
            wastesIndicatorOutlet.textColor = UIColor.red
            textViewOutlet.text = "Wastes container is full. Please press 'Clean' button!"
            print("Wastes full")
            deactivateButtons()
        default:
            print("Unexpected error.")
        }
    }
    
    func activateButtons() {
        espressoButtonOutlet.isEnabled = true
        americanoButtonOutlet.isEnabled = true
        latteButtonOutlet.isEnabled = true
        capuccionoButtonOutlet.isEnabled = true
        print("Buttons active")
    }
    
    func deactivateButtons() {
        espressoButtonOutlet.isEnabled = false
        americanoButtonOutlet.isEnabled = false
        latteButtonOutlet.isEnabled = false
        capuccionoButtonOutlet.isEnabled = false
        print("Buttons unactive")
    }

}

