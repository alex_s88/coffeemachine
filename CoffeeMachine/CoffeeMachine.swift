//
//  CoffeeMachine.swift
//  CoffeeMachine
//
//  Created by Alexandr Sopilnyak on 18.06.2018.
//  Copyright © 2018 Alexandr Sopilnyak. All rights reserved.
//

import UIKit

class CoffeeMachine: NSObject {
    private var water: Int //ml
    private var coffeeBeans: Int  //g
    private var milk: Int   //ml
    private var sugar: Int  //bricks
    private var wastes = 0
    
    init(water: Int, coffeeBeans: Int, milk: Int, sugar: Int) {
        self.water = water
        self.coffeeBeans = coffeeBeans
        self.milk = milk
        self.sugar = sugar
    }
    
    func makeEspresso(sugarBricks: Int, milkPortions: Int) {
        water -= 50
        coffeeBeans -= 25
        milk -= milkPortions
        sugar -= sugarBricks
        wastes += 1
        print("Espresso made")
    }
    
    func makeAmericano(sugarBricks: Int, milkPortions: Int) {
        water -= 150
        coffeeBeans -= 30
        milk -= milkPortions * 10 // 1 portion = 10 ml.
        sugar -= sugarBricks
        wastes += 1
        print("Americano made")
    }
    
    func makeLatte(sugarBricks: Int, milkPortions: Int) {
        water -= 50
        milk -= 150 + (milkPortions * 10)
        coffeeBeans -= 15
        sugar -= sugarBricks
        wastes += 1
        print("Latte made")
    }
    
    func makeCapuccino(sugarBricks: Int, milkPortions: Int) {
        water -= 50
        coffeeBeans -= 50
        milk -= 50 + (milkPortions * 10)
        sugar -= sugarBricks
        wastes += 1
        print("Capuccino made")
    }
    
    func addWater() {
        water = 1500
        print("Water = \(water)")
    }
    
    func addCoffeeBeans() {
        coffeeBeans = 250
        print("Coffee beans = \(coffeeBeans)")
    }
    
    func addMilk() {
        milk = 500
        print("Milk = \(milk)")
    }
    
    func addSugar() {
        sugar = 20
        print("Sugar = \(sugar)")
    }
    
    func clean() {
        wastes = 0
        print("Coffee machine clean")
    }
    
    func checkWater() -> Int {
        var level: Int
        if water < 50 {
            level = 0
        }
        else if water < 1000 {
            level = 1
        }
        else {
            level = 2
        }
        
        return level
    }
    
    func checkCoffee() -> Int {
        var level: Int
        if coffeeBeans < 30 {
            level = 0
        }
        else if coffeeBeans < 150 {
            level = 1
        }
        else {
            level = 2
        }
        
        return level
    }
    
    func checkMilk() -> Int {
        var level: Int
        if milk < 10 {
            level = 0
        }
        else if milk < 250 {
            level = 1 
        }
        else {
            level = 2
        }
        
        return level
    }
    
    func checkSugar() -> Int {
        var level: Int
        if sugar < 2 {
            level = 0
        }
        else if sugar < 10 {
            level = 1
        }
        else {
            level = 2
        }
        
        return level
    }
    
    func checkWastes() -> Int {
        var level: Int
        if wastes < 10 {
            level = 0
        }
        else if wastes <= 15 {
            level = 1
        }
        else {
            level = 2
        }
        
        return level
    }
    
    func status() -> (Int, Int, Int, Int, Int) {
        var coffeeMachineStatus = (water: 0, milk: 0, sugar: 0, coffee: 0, wastes: 0) // 0 - low, 1 - middle, 2 - full
        coffeeMachineStatus.water = checkWater()
        coffeeMachineStatus.coffee = checkCoffee()
        coffeeMachineStatus.milk = checkMilk()
        coffeeMachineStatus.sugar = checkSugar()
        coffeeMachineStatus.wastes = checkWastes()
        return coffeeMachineStatus
    }
}
